package Google;

import java.util.Set;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class WindowExamples {
	static WebDriver driver;

	@Test
	public void test_CloseAllWindowsExceptMainWindow() {
		driver = new FirefoxDriver();
		// It will open Naukri website with multiple windows
		driver.get("http://www.naukri.com/");
		
		// To get the main window handle
		String windowTitle= getCurrentWindowTitle();
		String mainWindow = getMainWindowHandle(driver);
		Assert.assertTrue((mainWindow));
		Assert.assertTrue(windowTitle.contains("Jobs - Recruitment"), "Main window title is not matching");
	}
		
	@Test
	public String getMainWindowHandle(WebDriver driver) {
		return driver.getWindowHandle();
	}

	@Test
	public String getCurrentWindowTitle() {
		String windowTitle = driver.getTitle();
		return windowTitle;
	}
}